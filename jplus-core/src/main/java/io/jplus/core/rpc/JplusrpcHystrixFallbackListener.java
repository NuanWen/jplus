/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.core.rpc;

import io.jboot.component.hystrix.JbootHystrixCommand;
import io.jboot.core.rpc.JbootrpcHystrixFallbackListener;

import java.lang.reflect.Method;

/**
 * Created by Administrator on 2018/3/3.
 */
public class JplusrpcHystrixFallbackListener implements JbootrpcHystrixFallbackListener {
    @Override
    public Object onFallback(Object proxy, Method method, Object[] args, JbootHystrixCommand command, Throwable exception) {
        return null;
    }
}
