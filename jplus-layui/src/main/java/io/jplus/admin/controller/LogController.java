/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.controller;

import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.Consts;
import io.jplus.admin.model.Log;
import io.jplus.admin.service.LogService;
import io.jplus.core.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresRoles;


@RequestMapping(value = "/admin/log", viewPath = Consts.BASE_VIEW_PATH + "admin/log")
public class LogController extends BaseController {

    @JbootrpcService
    LogService logService;


    public void list() {
        Columns columns = Columns.create();
        String orderBy = getPara("orderBy", "id desc");
        String logName = getPara("logName");
        if (StrKit.notBlank(logName)) {
            columns.eq("logname", logName);
        }
        String logType = getPara("logType");
        if (StrKit.notBlank(logType) && !"0".equals(logType)) {
            columns.eq("logtype", logType);
        }
        String beginTime = getPara("beginTime");
        String endTime = getPara("endTime");
        if (StrKit.notBlank(beginTime)) {
            columns.gt("createtime", beginTime);
        }
        if (StrKit.notBlank(endTime)) {
            columns.lt("createtime", endTime);
        }
        Page<Log> logPage = logService.paginateByColumns(getPageNumber(), getPageSize(), columns.getList(), orderBy);
        renderPageForBT(logPage);
    }

    public void detail(String logId) {
        Log optLog = logService.findById(logId);
        renderJson(optLog);
    }

    @RequiresRoles("admin")
    public void deleteAll() {
        boolean tag = logService.deleteAll();
        if (tag) {
            renderJson(Ret.ok());
            return;
        }
        renderJson(Ret.fail(Consts.RET_MSG, "日志清空失败！"));
    }
}
