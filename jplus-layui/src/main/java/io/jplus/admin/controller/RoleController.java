/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.paragetter.Para;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.db.model.Columns;
import io.jboot.utils.StringUtils;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.Consts;
import io.jplus.admin.model.Dept;
import io.jplus.admin.model.Role;
import io.jplus.admin.service.DeptService;
import io.jplus.admin.service.RoleService;
import io.jplus.core.base.BaseController;
import io.jplus.core.render.DataTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RequestMapping(value = "/admin/role", viewPath = Consts.BASE_VIEW_PATH + "admin/role")
public class RoleController extends BaseController {

    @JbootrpcService
    RoleService roleService;

    @JbootrpcService
    DeptService deptService;


    public void list() {
        Columns columns = Columns.create();
        String roleName = getPara("roleName");
        if (StrKit.notBlank(roleName)) {
            columns.like("name", roleName);
        }
        String orderBy = getPara("orderBy", "id");
        Page<Role> page = roleService.paginateByColumns(getPageNumber(), getPageSize(), columns.getList(), orderBy);
        renderJson(new DataTable<Role>(page));
    }


    public void add() {
        render("add.html");
    }

    public void edit(Integer roleId) {
        if (roleId == null) {
            renderJson(Ret.fail(Consts.RET_MSG, "数据不存在！"));
            return;
        }
        Role role = roleService.findById(roleId);

        if (StringUtils.isNotBlank(role.getPid())) {
            Role tmpRole = roleService.findById(role.getPid());
            role.put("pName", tmpRole.getName());
        }

        if (StringUtils.isNotBlank(role.getDeptid())) {
            Dept dept = deptService.findById(role.getDeptid());
            role.put("deptName", dept.getFullname());
        }

        setAttr("role", role);
        render("edit.html");
    }

    @Before(POST.class)
    public void save(@Para("") Role role) {
        boolean tag = roleService.saveOrUpdate(role);
        if (tag) {
            renderAjaxResultForSuccess();
        } else {
            renderAjaxResultForError("保存失败！");
        }

    }

    @Before(POST.class)
    public void update(@Para("") Role role) {
        boolean tag = roleService.saveOrUpdate(role);
        if (tag) {
            renderAjaxResultForSuccess();
        } else {
            renderAjaxResultForError("更新失败！");
        }

    }

    public void tree() {
        List<Role> roleList = roleService.findAll();
        renderJson(buildTree(roleList));
    }

    public List<Map<String, Object>> buildTree(List<Role> roleList) {
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (Role role : roleList) {
            Map<String, Object> map = new HashMap(4);
            map.put("id", role.getId());
            map.put("pid", role.getPid());
            map.put("name", role.getName());
            map.put("open", false);
            mapList.add(map);
        }
        Map parent = new HashMap();
        parent.put("id", 0);
        parent.put("pid", 0);
        parent.put("name", "顶级");
        parent.put("open", false);
        parent.put("checked", true);
        mapList.add(parent);
        return mapList;
    }

    public void delete(Integer roleId) {
        boolean tag = roleService.deleteById(roleId);
        if (tag) {
            renderAjaxResultForSuccess();
        } else {
            renderAjaxResultForError("删除失败！");
        }
    }
}
