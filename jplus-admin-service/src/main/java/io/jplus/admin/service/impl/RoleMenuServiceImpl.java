package io.jplus.admin.service.impl;

import io.jboot.aop.annotation.Bean;
import io.jboot.core.rpc.annotation.JbootrpcService;
import io.jboot.service.JbootServiceBase;
import io.jplus.admin.model.RoleMenu;
import io.jplus.admin.service.RoleMenuService;

import javax.inject.Singleton;

@Bean
@Singleton
@JbootrpcService
public class RoleMenuServiceImpl extends JbootServiceBase<RoleMenu> implements RoleMenuService {

}