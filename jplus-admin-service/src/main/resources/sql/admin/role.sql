#sql("findByUserId")
SELECT
  r.id,
  r.deptid,
  r.name,
  r.pid,
  r.tips,
  r.version,
  r.num
FROM jp_role r, jp_user_role ur
WHERE r.id = ur.roleid
      AND ur.userid = ?
#end